
try:
    file = open("text_1.txt", "w")
    # ...
finally:
    file.close()
# r Открывает файл для чтения. (по умолчанию)
# w "Открывает файл для записи. Создает новый файл, если он не существует, или обрезает файл,
# если он существует."
# x "Открывает файл для эксклюзивного создания. Если файл уже существует, операция не
# выполняется."
# a "Открывает файл для добавления в конец файла без его усечения. Создает новый файл, если
# он не существует."
# t Открывается в текстовом режиме. (по умолчанию)
# b Открывается в бинарном режиме.
# + Открывает файл для обновления (чтения и записи)

with open("text_1.txt") as file:
    pass

