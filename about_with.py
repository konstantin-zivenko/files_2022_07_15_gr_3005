"""with <obj> as name:
    <obj>.__enter__()


    <obj>.__exit__()"""

class my_file_reader():
    def __init__(self, file_path):
        self.__path = file_path
        self.__file_object = None

    def __enter__(self):
        self.__file_object = open(self.__path)
        return self.__file_object

    def __exit__(self, type, val, tb):
        self.__file_object.close()

with my_file_reader("text_2.txt") as reader:
    print(reader.readlines())
