with open("text_2.txt", "w") as f:
    f.write("some string")


str_list = [
    'string 1',
    'string 2',
    'string 3'
]

with open("text_2.txt", "w") as f:
    f.writelines(str_list)