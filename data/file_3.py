with open("text_1.txt") as f:
    line = f.readline()
    while line != "":
        print(line, end="")
        line = f.readline()

with open("text_1.txt") as f:
    for i in f.readlines():
        print(i, end="")


with open("text_1.txt") as f:
    for i in f:
        print(i, end="")

with open("text_1.txt") as f:
    itr = iter(f)
    print(next(itr))
    print(next(itr))